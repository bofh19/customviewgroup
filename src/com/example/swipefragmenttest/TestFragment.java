package com.example.swipefragmenttest;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

@SuppressLint({ "NewApi", "ValidFragment" })
public class TestFragment extends Fragment {
	private static int i = 0;

	@SuppressLint("ResourceAsColor")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		i = i + 1;
		View view;
		if (i == 2) {
			view = inflater.inflate(R.layout.fragment_2, container, false);
		} else {
			view = inflater.inflate(R.layout.fragment_rssitem_detail,
					container, false);
		}
		TextView txtview = (TextView) view.findViewById(R.id.detailsText);
		txtview.setText("Test fragment " + " ----- " + i);
		return view;
	}
}