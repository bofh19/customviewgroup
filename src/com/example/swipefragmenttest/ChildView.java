package com.example.swipefragmenttest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class ChildView extends View {

	private int mColor = Color.RED; // default

	public ChildView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ChildView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ChildView(Context context) {
		super(context);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawColor(mColor);
		canvas.drawText("Child", 10, 10, new Paint());
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// normally you would override this to measure the children based on
		// the dimensions you get from the parent
		// in the particular case of my ViewGroup there is no need to
		// override this because the default implementation will create the
		// right width/height values.
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		Log.e("XZX", "" + getMeasuredHeight() + " " + getMeasuredWidth());
	}

	public void setColor(int color) {
		mColor = color;
		requestLayout();
		invalidate();
	}

}