package com.example.swipefragmenttest;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;

public class ParentViewGroup extends ViewGroup {

	public ParentViewGroup(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ParentViewGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ParentViewGroup(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int width = MeasureSpec.getSize(widthMeasureSpec);
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		final int height = MeasureSpec.getSize(heightMeasureSpec);
		final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int childWidthSize = getMeasuredWidth();
		int childHeightSize = getMeasuredHeight();
		int calculatedWidth, calculatedHeight;
		if (widthMode == MeasureSpec.UNSPECIFIED) {
			calculatedWidth = 800; // we don't have any constraints so
									// assign
									// an arbitrary value(normally you would
									// see how big the children want to be)
		} else {
			// just use the width
			calculatedWidth = width;
		}
		if (heightMode == MeasureSpec.UNSPECIFIED) {
			calculatedHeight = 800;
		} else {
			calculatedHeight = height;
		}
		setMeasuredDimension(calculatedWidth, calculatedHeight);
		// measure the children giving them the proper constraints.
		getChildAt(0).measure(
				MeasureSpec.makeMeasureSpec(calculatedWidth / 2,
						MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(calculatedHeight / 2,
						MeasureSpec.EXACTLY));
		getChildAt(1).measure(
				MeasureSpec.makeMeasureSpec(calculatedWidth / 2,
						MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(calculatedHeight / 2,
						MeasureSpec.EXACTLY));
		Log.e("XZX", "" + + calculatedHeight + "  " + calculatedWidth);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		// setup where the children will be placed in their parent(this
		// ViewGroup)
		// the first child
		getChildAt(0).layout(0, 0, getMeasuredWidth() / 2,
				getMeasuredHeight() / 2);
		// the second child
		getChildAt(1).layout(getMeasuredWidth() / 2,
				getMeasuredHeight() / 2, getMeasuredWidth(),
				getMeasuredHeight());
	}

}

