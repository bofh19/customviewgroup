package com.example.swipefragmenttest;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

public class SwipeFragmentTest extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_swipe_fragment_test);
		FrameLayout fr1 = (FrameLayout) findViewById(R.id.frame_layout_1);
		FrameLayout fr2 = (FrameLayout) findViewById(R.id.frame_layout_2);
		TestFragment fg = new TestFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(R.id.frame_layout_1, fg);
		TestFragment fg2 = new TestFragment();
		ft.replace(R.id.frame_layout_2, fg2);
		ft.commit();
		
		fr1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
			}
		});
//		TextView b = (TextView) findViewById(R.id.txtview);
//		b.setOnTouchListener(new OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				Log.d("-------------",""+event.getX()+" --- "+event.getY());
//				return false;
//			}
//		});
//		
//		fr1.setY(0);
//		
//		fr2.setY(90);
//		
//		b.setOnDragListener(new OnDragListener() {
//			
//			@Override
//			public boolean onDrag(View v, DragEvent event) {
//				Log.d("-------------",""+event.getX()+" --- "+event.getY());
//				return false;
//			}
//		});
//		ParentViewGroup pvg = new ParentViewGroup(this);
//		ChildView b = new ChildView(this);		
//		b.setColor(Color.GREEN);
//		pvg.addView(b);
//		b = new ChildView(this);
//		b.setColor(Color.BLUE);
//		pvg.addView(b);
//		setContentView(pvg);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.swipe_fragment_test, menu);
		return true;
	}

}
