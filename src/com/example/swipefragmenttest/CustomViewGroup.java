package com.example.swipefragmenttest;

import android.content.Context;
import android.support.v4.view.VelocityTrackerCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;

public class CustomViewGroup extends ViewGroup{

	public CustomViewGroup(Context context) {
		super(context);
	}
	
	public CustomViewGroup(Context context, AttributeSet attrs){
		super(context,attrs);
	}
	
	public CustomViewGroup(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int width = MeasureSpec.getSize(widthMeasureSpec);
		final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		final int height = MeasureSpec.getSize(heightMeasureSpec);
		final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int childWidthSize = getMeasuredWidth();
		int childHeightSize = getMeasuredHeight();
		int calculatedWidth, calculatedHeight;
		if (widthMode == MeasureSpec.UNSPECIFIED) {
			calculatedWidth = 800; // we don't have any constraints so
									// assign
									// an arbitrary value(normally you would
									// see how big the children want to be)
		} else {
			// just use the width
			calculatedWidth = width;
		}
		if (heightMode == MeasureSpec.UNSPECIFIED) {
			calculatedHeight = 800;
		} else {
			calculatedHeight = height;
		}
		setMeasuredDimension(calculatedWidth, calculatedHeight);
		// measure the children giving them the proper constraints.
		getChildAt(0).measure(
				MeasureSpec.makeMeasureSpec(calculatedWidth ,
						MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(calculatedHeight,
						MeasureSpec.EXACTLY));
		getChildAt(1).measure(
				MeasureSpec.makeMeasureSpec(calculatedWidth,
						MeasureSpec.EXACTLY),
				MeasureSpec.makeMeasureSpec(calculatedHeight,
						MeasureSpec.EXACTLY));
		Log.e("XZX", "" + + calculatedHeight + "  " + calculatedWidth);
	}
 
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		//super.onLayout(changed, l, t, r, b);
		// setup where the children will be placed in their parent(this
		// ViewGroup)
		// the first child;
		getChildAt(1).layout(0, 0, -getMeasuredWidth(),
				-getMeasuredHeight());
		// the second child
		getChildAt(0).layout(0,
				50, getMeasuredWidth(),
				getMeasuredHeight());
		getChildAt(0).bringToFront();
        getChildAt(1).bringToFront();
		requestLayout();
		invalidate();
	}
	
	private VelocityTracker mVelocityTracker = null;
	
	 @Override
	 public boolean onInterceptTouchEvent(MotionEvent ev) {
		 Log.d("--on intercept touch event --- ",""+ev.getAction());
		return onTouchEvent(ev);
		 
	 }
	 
	 @Override
	    public boolean onTouchEvent(MotionEvent event) {
	        int index = event.getActionIndex();
	        int action = event.getActionMasked();
	        int pointerId = event.getPointerId(index);
	        Log.d("",event.toString());
	        switch(action) {
	            case MotionEvent.ACTION_DOWN:
	                if(mVelocityTracker == null) {
	                    // Retrieve a new VelocityTracker object to watch the velocity of a motion.
	                    mVelocityTracker = VelocityTracker.obtain();
	                }
	                else {
	                    // Reset the velocity tracker back to its initial state.
	                    mVelocityTracker.clear();
	                }
	                // Add a user's movement to the tracker.
	                mVelocityTracker.addMovement(event);
	                break;
	            case MotionEvent.ACTION_MOVE:
	                mVelocityTracker.addMovement(event);
	                // When you want to determine the velocity, call 
	                // computeCurrentVelocity(). Then call getXVelocity() 
	                // and getYVelocity() to retrieve the velocity for each pointer ID. 
	                mVelocityTracker.computeCurrentVelocity(1000);
	                // Log velocity of pixels per second
	                // Best practice to use VelocityTrackerCompat where possible.
//	                Log.d("", "X velocity: " + 
//	                        VelocityTrackerCompat.getXVelocity(mVelocityTracker, 
//	                        pointerId));
//	                Log.d("", "Y velocity: " + 
//	                        VelocityTrackerCompat.getYVelocity(mVelocityTracker,
//	                        pointerId));
	                int x=(int)Math.round(event.getX());
	                int y=(int)Math.round(event.getY());
	                getChildAt(1).layout(0,y-getMeasuredHeight(), getMeasuredWidth(), getMeasuredHeight());
	                getChildAt(0).layout(0,50, getMeasuredWidth(),getMeasuredHeight());
	                getChildAt(0).setOnTouchListener(new OnTouchListener() {
						
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							mVelocityTracker.addMovement(event);
							return false;
						}
					});
	                Log.d("","x---"+x+" y----"+y);
	                break;
	            case MotionEvent.ACTION_UP:
	            case MotionEvent.ACTION_CANCEL:
	                // Return a VelocityTracker object back to be re-used by others.
	                mVelocityTracker.recycle();
	                break;
	        }
	        return true;
	    }
	

}
